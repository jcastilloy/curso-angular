export class DestinoViaje{
    private selected: boolean;
    public servicios: string[];
    public id;

    // nombre: string;
    // imagenUrl: string;

    // constructor(n: string, u: string){
    //     this.nombre = n;
    //     this.imagenUrl = u;
    // }
    constructor(public nombre: string, public u: string, public votes: number = 0){
        this.servicios = ['pileta', 'desayuno'];
    }

    isSelected(): boolean{
        return this.selected;
    }

    setSelected(s: boolean){
        this.selected = s;
    }
    voteUp(){
        this.votes++;
    }
    voteDown(){
        this.votes--;
    }
}
