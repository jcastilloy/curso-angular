import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store, State } from '@ngrx/store';
import { AppState, APP_CONFIG, Appconfig, db } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';


@Injectable()
export class DestinosApiClient{
    destinos: DestinoViaje[];
    // current: Subject<DestinoViaje>= new BehaviorSubject<DestinoViaje>(null);

    // constructor(private store: Store<AppState>){
        constructor(
            private store: Store<AppState>, 
            @Inject(forwardRef(() => APP_CONFIG)) private config: Appconfig, 
            private http: HttpClient
        ) {
        // this.destinos=[];
        this.store
        .select(state => state.destinos)
        .subscribe((data) => {
            console.log('destino sub store');
            console.log(data);
            this.destinos = data.items;

        });
        this.store
        .subscribe((data) =>{
            console.log('all store');
            console.log(data);
        });
    };

    // add(a: DestinoViaje){
    //     this.store.dispatch(new NuevoDestinoAction(a));
    //     // this.destinos.push(a);
    // }

    add(d: DestinoViaje){
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', {nuevo: d.nombre}, {headers: headers});
        this.http.request(req).subscribe((data: HttpResponse<{}>) =>{
            if(data.status === 200){
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDB = db;
                myDB.destinos.add(d);
                console.log('todos los destin os de la db!');
                myDB.destinos.toArray().then(destinos => console.log(destinos))
            }
        });
    }

    // getAll(): DestinoViaje[]{
    //     return this.destinos;
    // }
    getById(id: string): DestinoViaje{
        return this.destinos.filter(function(d) {return d.id === id; })[0];
    }

    elegir(d: DestinoViaje){
        this.store.dispatch(new ElegidoFavoritoAction(d));
        // this.destinos.forEach(x => x.setSelected(false));
        // d.setSelected(true);
        // this.current.next(d);
    }

    // suscribeOnChange(fn){
    //     this.current.subscribe(fn)
    // }
}