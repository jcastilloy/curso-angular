"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ListaDetinosComponent = void 0;
var core_1 = require("@angular/core");
var destino_viaje_model_1 = require("./../models/destino-viaje.model");
var ListaDetinosComponent = /** @class */ (function () {
    function ListaDetinosComponent() {
        this.destinos = [];
    }
    ListaDetinosComponent.prototype.ngOnInit = function () {
    };
    ListaDetinosComponent.prototype.guardar = function (nombre, url) {
        this.destinos.push(new destino_viaje_model_1.DestinoViaje(nombre, url));
        console.log(this.destinos);
        return false;
    };
    ListaDetinosComponent = __decorate([
        core_1.Component({
            selector: 'app-lista-detinos',
            templateUrl: './lista-detinos.component.html',
            styleUrls: ['./lista-detinos.component.css']
        })
    ], ListaDetinosComponent);
    return ListaDetinosComponent;
}());
exports.ListaDetinosComponent = ListaDetinosComponent;
