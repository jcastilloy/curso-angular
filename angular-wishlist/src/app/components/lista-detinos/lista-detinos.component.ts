import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from'../../models/destino-viaje.model';
import{ DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store, State } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';
import { isNgTemplate } from '@angular/compiler';

@Component({
  selector: 'app-lista-detinos',
  templateUrl: './lista-detinos.component.html',
  styleUrls: ['./lista-detinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDetinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  update: string[];
  all

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.update = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if(d != null){
          this.update.push('se ha elegido a '+ d.nombre);
        }
      });
      store.select(State => State.destinos.items).subscribe(items => this.all = items);

    // this.destinosApiClient.suscribeOnChange((d: DestinoViaje) => {
    //   if(d != null){
    //     this.update.push('se ha elegido a '+ d.nombre);
    //   }
    // });
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    // this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e: DestinoViaje){
    
    //desmarca todos los demas en el array de elegidos
    // this.destinosApiClient.forEach(function (x) {x.setSelected(false);});
    //se marca el elegido
    // debugger.setSelected(true);

    // this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    // e.setSelected(true);
    
    this.destinosApiClient.elegir(e);
    // this.store.dispatch( new ElegidoFavoritoAction(e));
    
  }

}
