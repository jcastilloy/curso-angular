import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { APP_CONFIG, Appconfig } from 'src/app/app.module';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;

  minLongitud =3;

  serchResult: string[];

  constructor(fb:FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: Appconfig) {
    //inicializar
    this.onItemAdded = new EventEmitter();
    //vinculacion con tag html
    this.fg = fb.group({
      // nombre: ['', Validators.required],
      nombre: ['', Validators.compose([ 
                                Validators.required, 
                                this.nombreValidator,
                                this.nombreValidatorParametrizable(this.minLongitud)
                              ])],
      url: ['', Validators.required]
    });

    //observador de tipeo
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio en el formulario: ', form);
    });
   }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(
              this.config.apiEndPoint + '/ciudades?q='+ text
            )
          )
      ).subscribe(
        ajaxResponse => this.serchResult = ajaxResponse.response
        )
  }

  guardar(nombre: string, url: string): boolean{
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  //required: true -> esta es la sintaxis del tipo de datos de retorno, un tipo de dato compuesto
  nombreValidator(control: FormControl): {[s: string]: boolean} {
    let l = control.value.toString().trim().length;
    if(l>0 && l<5){
      return {invalidNombre: true}
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): {[s: string]: boolean} | null => {
      let l = control.value.toString().trim().length;
      if(l>0 && l<minLong){
        return {invalidNombre: true}
      }
      return null;
    }
  }

}
