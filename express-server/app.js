var express = require('express');
cors  = require('cors');
var app = express();
var bodyParser = require('body-parser');


app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
//app.use(express.json());

app.listen(3000, () => console.log("server running on port 3000"));

app.get('/', function (req, res) {
    return res.json({
      "message":"Express is working"
    })
  })

var ciudades = ["Paris", "Barcelona", "Barranquilla", "Montevideo", "Santiago de Chile", "Mexico DF", "Nueva York"];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().lastIndexOf(req.query.q.toString().toLowerCase()) > -1)));

var misDestinos=[];

app.get("/my", (req, res, next) => res.json(misDestinos));

app.post("/my", (req, res, next) => {
    // console.log(req.body);
    // console.log(req.headers);
    //misDestinos = req.body;
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});

app.get("/api/translation", (req, res, next) => res.json([
  { lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang }
]));